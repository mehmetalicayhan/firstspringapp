package com.example.firstspringapp.model;

public enum Gender {
    FEMALE,
    MALE
}
