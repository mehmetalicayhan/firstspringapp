package com.example.firstspringapp.model;

import com.example.firstspringapp.util.UniqueId;
import lombok.Data;
import lombok.AllArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.UUID;

@AllArgsConstructor
@Data
public class User {

  @UniqueId
  private UUID id;

  @Size(min = 1, max = 10)
  @NotNull
  private String name;

  @Size(min = 1, max = 10)
  @NotNull
  private String surname;

  @NotNull
  private Gender gender;
}
