package com.example.firstspringapp.service;

import com.example.firstspringapp.model.User;
import com.example.firstspringapp.repository.UserRepositoryImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService {
  UserRepositoryImpl userRepository;

  public UserService(UserRepositoryImpl userRepository) {
    this.userRepository = userRepository;
  }

  public List<User> getAllUsers() {
    return userRepository.getAllUsers();
  }

  public void createUser(User newUser) {
    userRepository.createUser(newUser);
  }

  public User getUserById(UUID id) {

    return userRepository.getUserById(id);
  }
  public User getUserByName(String name) {
    return userRepository.getUserByName(name);
  }

  public void updateUser(String id, User user) {
    userRepository.updateUser(id, user);
  }

  public void deleteUser(UUID id) {
    userRepository.deleteUser(id);
  }
}
