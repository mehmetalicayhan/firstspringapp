package com.example.firstspringapp.controller;

import com.example.firstspringapp.model.User;
import com.example.firstspringapp.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService){

        this.userService = userService;
    }
    @PostMapping("/add")
    public void createUser(@Valid @RequestBody User newUser){
        userService.createUser(newUser);
    }


    @GetMapping()
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }


    @GetMapping("/{id}")
    public User getUserById(@PathVariable String id){
        return userService.getUserById(UUID.fromString(id));
    }


    @PutMapping("/{id}")
    public void  updateUser(@PathVariable String id, @RequestBody @Valid User newUser){
        userService.updateUser(id,newUser);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable String id){
        userService.deleteUser(UUID.fromString(id));
    }
}
