package com.example.firstspringapp.repository;

import com.example.firstspringapp.model.User;

import java.util.List;
import java.util.UUID;

public interface UserRepository {
  public List<User> getAllUsers();

  public void createUser(User newUser);

  public User getUserById(UUID id);

  public User getUserByName(String name);

  public void updateUser(String id, User user);

  public void deleteUser(UUID id);
}
