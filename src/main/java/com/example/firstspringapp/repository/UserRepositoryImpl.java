package com.example.firstspringapp.repository;

import com.example.firstspringapp.model.Gender;
import com.example.firstspringapp.model.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Repository
public class UserRepositoryImpl implements UserRepository {
  public List<User> users =
      new ArrayList<>(
          Arrays.asList(
              new User(UUID.randomUUID(), "Ali", "Çayhan", Gender.MALE),
              new User(UUID.randomUUID(), "Ahmet", "Yılmaz", Gender.MALE),
              new User(UUID.randomUUID(), "Ayşe", "Kaplan", Gender.FEMALE),
              new User(UUID.randomUUID(), "Münevver", "Aslan", Gender.FEMALE)));

  public List<User> getAllUsers() {
    return users;
  }

  public void createUser(User newUser) {
    newUser.setId(UUID.randomUUID());
    users.add(newUser);
  }

  public User getUserById(UUID id) {
    return users.stream().filter(user -> user.getId().equals(id)).findFirst().orElse(null);
  }

  public User getUserByName(String name) {
    return users.stream().filter(user -> user.getName().equals(name)).findFirst().orElse(null);
  }

  public void updateUser(String id, User user) {
    for (int i = 0; i < users.size(); ++i) {
      User oldUser = users.get(i);
      if (oldUser.getId().toString().equals(id)) {
        users.set(i, user);
        users.get(i).setId(UUID.fromString(id));
        break;
      }
    }
  }

  public void deleteUser(UUID id) {
    users.removeIf(u -> u.getId().equals(id));
  }
}
