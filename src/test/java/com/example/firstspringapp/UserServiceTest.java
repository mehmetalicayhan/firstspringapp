package com.example.firstspringapp;

import com.example.firstspringapp.model.Gender;
import com.example.firstspringapp.model.User;
import com.example.firstspringapp.repository.UserRepositoryImpl;

import com.example.firstspringapp.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

  private UserRepositoryImpl userRepository;
  private UserService userService;

  @BeforeEach
  public void setUp() throws Exception {
    userRepository = Mockito.mock(UserRepositoryImpl.class);
    userService = new UserService(userRepository);
  }

  @Test
  public void shouldReturnAllUsers() {
    List<User> expectedUsers = new ArrayList<User>();
    Mockito.when(userRepository.getAllUsers()).thenReturn(expectedUsers);
    Assertions.assertEquals(expectedUsers, userService.getAllUsers());
  }

  @Test
  public void shouldReturnUserWithId() {
    User user = new User(UUID.randomUUID(), "name", "surname", Gender.MALE);
    Mockito.when(userRepository.getUserById(user.getId())).thenReturn(user);
    //    User expectedUser = userSpy.getUserById(actualUser.getId());

    Assertions.assertEquals(user, userService.getUserById(user.getId()));
  }

  @Test
  public void shouldAddNewUser() {
    User user = new User(UUID.randomUUID(), "test", "test", Gender.MALE);
    userRepository.createUser(user);
    Mockito.verify(userRepository).createUser(user);
  }

  @Test
  public void shouldUpdateUser() {
    User user = new User(UUID.randomUUID(), "name", "surname", Gender.MALE);
    userService.createUser(user);
    user.setName("Test2");
    userService.updateUser(user.getId().toString(), user);
    Mockito.verify(userRepository).updateUser(user.getId().toString(), user);
  }

  @Test
  public void shouldDeleteUser() {
    User user = new User(UUID.randomUUID(), "test", "test", Gender.MALE);
    userRepository.createUser(user);
    userService.deleteUser(user.getId());
    Assertions.assertNull(userRepository.getUserById(user.getId()));
  }
}
